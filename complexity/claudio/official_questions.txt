What is a problem?
A problem is a mapping from a set of inputs to a set of outputs.

What is a decision problem?
A problem is called decision problem if the set of outputs only consists of the two elements "yes" and "no".
Problems can be naturally translated into decision problems.

What is an Algorithm?
An Algorithm consists of a finite number of instructions and fulfills folowing five properties:
 - Finiteness: For every input, an algorithm always terminates after a finite number of steps.
 - Defininiteness: Each step precisely defined, actions to be carried out are unambiguously specified.
 - Input 
 - Output
 - Effectiveness: sufficently basic instruction to be carried our exactly and in finite time
What maybe is not that intuitive is that programms which use endless loops are no algorithms.
This is somehow weird as you practically have e.g. web services and embedded systems running 'for ever'.

What is the travelling salesman problem (TSP) ?
Basically: 
A set of cities is given. Traversing from one city to another has a defined cost.
The costs vary. A sales man wants to sell his products in all citys.
Find a path with minimal costs such that the salesman traverses through all cities and ends at the city he started at.
(In)Formal:
Input: n x n Distance Matrix for n cities
Output: Permutation P with minimal costs, selected from the set of all possible permutations for D

What is the TSP decision problem (TSPd) ? How could it be solved with an algorithm for TSP?
Input: Algorithm A which solves TSP, Distance Matrix D, Random Cost Value RC
Output:	Yes, if the minimal costs for D calculated by A are less equal RC
	No, therwise.

How could TSP be solved with an algorithm for TSPd?
Two phases are required:
A	Start with a random expected cost value RC.
	Choose it by e.g. using a random valid route for Distance Matrix D.
	Use an binary search approach to get the minimal cost value (MC)
B	Now that MC is known we start modifying D and check if MC still holds for it.
	Example: We dramatically increase the costs for traversing from city X to Y.
	If the minimum costs MC for modfied D did not change we know: in our route with minimal costs there is no traversion from city X to Y.
	But increasing the cost for one city only is not enough. The costs for all but one need to be increased.
	Assume: From city A we can traverse to cities B,C,D,E. We increase the costs from traversion from A to C,D,E.
	If MC did not change we know the minimum route includes traversion from A to B. 
	Else if MC did not change we repeat the process and increase the costs for traversion from A to B,D,E.
	If MC did not change we know the minimum costs route includes traversion from A to C.
	Else...

Explain the complexity of these approaches
The bruteforce solution to the TSP requires you to calculate the costs for (n-1)! routes for an n x n Distance Matrix.
Since this is requires more steps then O(n^k) it belongs to O(n!).
 for each city x {
	for each city y {
		identify traversion x to y
		increase the costs of all other traverions starting at x
		check: minimal costs still given ?
		yes : continues
		no: break
	}
}

What is Complexity w.r.t. an algorithm?
## needs improvement
Algorithms can be categorized with respect to their performance for an increasing input size n.
This categorization basically identifies to which complexity class the algorithm belongs.
Good algorithms are said to solve an algorithm in polynomial time.
Complexity can be used to specify how performant an algorithm is.
The complexity of algorithms can be measured/estimated and categorized.
The categorization depe which are all dependent on the input size n processed by the algorithm.
Fast algorithms belong have are said to be solved in polynomial time.


what is the running time of an algorithm?
Each algorithm consists of a finite number of step. Hence it can be estimated how many steps an algorithm will need to process an input of size n in worst case.
This is basically the running time of an algorithm. Afterwards you can categorize the algorithm according to one of the big O classes which show how the algorithm
behaves for an increasing input size n.

TSP vs TSPd, which is more complex?
I would say that TSPd is less complex. The reason is that it already takes as input an algorithm/program which calculates the minimal cost route for a distance mactrix D.

What is decidability?
Decidabilty is the fact that an algorithm will always be able to match any input I to an output O.

Can all decision problems be solved by an algorithm?
The halting problem cannot be solved by an algorithm.

Name an undecidable decision problem.
The halting problem.

Proof that it is undecidable.
The proof works by deriving a contradiction. 
We assume that there actually is an algorithm that checks whether an programm P will halt for a given input or not.
Now we have another program absurd:
	Input is a Program P
	if HALT(P, P) then go into endless loop 
	else if !HALT(P, PI) then return succesfully
Contradiction:
	we execute ABSURD(ABSURD);
	if ABSURD halts, then this causes absurd to go into an endless loop and does not halt. but HALT(ABSURD, ABSURD) said it would halt. 
	else if ABSURD does not halt according to HALT(ABSURD, ABSURD), then this causes absurd to return success. But its said not to halt.

Explain the big O notation
Assuming you have an algorithm A which has a growth function f. For a given n this function will return a value specifying the numer of steps needed to process the input.
Big O is about finding a second function g which serves as an upper bound to the grwoth of f. It means that from a certain input size n onwards no matter how f looks like, it will not grow faster then g. This totally makes sense as: if the time/number of steps needed to process an input of size n, then if n just gets freaking big enough any other constants or multiplication that let f and g differ will be that unimportant that they grow with the same speed.

What is the big omega notation?
Big Omega specifies a lower bound for the growth function of an Algorithm.

What is a touring machine?
Consists of:
	an infinite tape which serves as sequential storage
	a head to read the tape at a position/index/address
	finite set of states (one initial, n final)
	working alphabet (letters to write/read on the tape), including blank symbol
	transition function: 
		input: symbol read at current head position + current state
		output: new symbol to write at current head position + new state + head movement:LRN

What is a nondeterministic touring machine?
	Basically the same as a regulat touring machine. The difference lies in the transition function.
	Normally the tranistion function unambiguously defines one action for a given input in a given state. 
	In a nondeterministic TM for one input and one state multiple actions are possible.
	The machine is free to chose which one to perform.
Nondeterministic TMs can solve any problem in polynomial time. It is quiet easy.
Yet I am not completely sure how the are supposed to solve it technically.
Either by parallelising the actions for an input. These actions are performed concurrent.
Finally results of concurrent processes are compared.
Or they luckily choose the right actions to solve it in polynomial time.

What is P?
	A decision problem that can be solved in polynomial time. 
	That is, given an instance of the problem, the answer yes or no can be decided in polynomial time.

What is NP?
	A decision problem where instances of the problem for which the answer is yes have proofs that can be verified in polynomial time.
	This means that if someone gives us an instance of the problem and a certificate (sometimes called a witness) to the answer being yes, we can check that it is correct in polynomial time. E.g. SAT. If we know the answer to an input we can easily check whether it is true.

How are P and NP related?
	P is a subset of NP.

What is NP-complete?
	NP-complete means both NP-hard and in NP.
	Sometimes it's not important to mention that something is in NP even if it is, so NP-hard is said instead.
	A subset of NP. These problems are said to only be nodeterministically solvable in polynomial time.
	Any yet undiscovered algorithm deterministically solving any problem which is NP complete can be transformed to determin. solve any problem in NP in polynomial time and proof that P=NP.
	An NP problem X for which it is possible to reduce any other NP problem Y to X in polynomial time. 
	Intuitively this means that we can solve Y quickly if we know how to solve X quickly. 
	Precisely, Y is reducible to X if there is a polynomial time algorithm f to transform instances y of Y to instances x = f(y) of X in polynomial time with the property that the answer to y is yes if and only if the answer to f(y) is yes.
	Example: 3SAT

What is NP-hard?
	E.g. Halting Problem.
	A problem is NP-hard if and only if it’s “at least as” hard as an NP-complete problem.
	The more conventional Traveling Salesman Problem of finding the shortest route is NP-hard, not strictly NP-complete.

What is polynomial reduction?
	In computational complexity theory, a polynomial-time reduction is a method of solving one problem by means of a hypothetical subroutine for solving a different problem (that is, a reduction), that uses polynomial time excluding the time within the subroutine. 

Explain 'SAT can be reduced to 3SAT'
	each SAT clause has 1, 2, 3 or more variables
	the 3 variable clause can be copied with no issue
	the 1 and 2 variable clauses {a1} and {a1,a2} can be expanded to {a1,a1,a1} resp. {a1,a2,a1}
	the clause with more than 3 variables  {a1,a2,a3,a4,a5} can be expanded to 
	{a1,a2,s1}{!s1,a3,s2}{!s2,a4,a5} with s1 and s2 new variables whose value will depend on which variable in the original clause is true

Explain 'SAT less equal VC'
	Let's introduce a variable xi for every node i, representing the condition that the node is part of the vertex cover. 
	Then for every edge {v,w} we introduce the clause xv∨xw.
	We have the additional condition x1+x2+… + xn≤k. We can combine an addition circuit to compute the bits of the sum with a comparator circuit to enforce the inequality.
	The CNF of the resulting circuit has only polynomially many clauses.

Explain SAT
Explain 3SAT
Explain VC
