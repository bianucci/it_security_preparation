1.	Describe the following terms briefly in 3-4 sentences.
	Firewall
A software operating on layer 3 or 4 or 5. Uses rules which can be chained to block certain incoming or outgoing traffic.
	Packet Filter
Packet filters act by inspecting the "packets" which are transferred between computers on the Internet. If a packet does not match the packet filter's set of filtering rules, the packet filter will drop (silently discard) the packet or reject it (discard it, and send "error responses" to the source). Conversely, if the packet matches one or more of the programmed filters, the packet is allowed to pass. Filterting based on ports is also included.
	Application Level Gateway
Could be either a NAT which supports different cone configurations (full/restricted/port restricted) or inspects packets and acts as firewall blocking traffic which violates certain rules. E.g. blacklisted website a.s.o.
	Demilitarized Zone
In computer security, a DMZ or demilitarized zone (sometimes referred to as a perimeter network) is a physical or logical subnetwork that contains and exposes an organization's external-facing services to a larger and untrusted network, usually the Internet. The purpose of a DMZ is to add an additional layer of security to an organization's local area network (LAN); an external network node only has direct access to equipment in the DMZ, rather than any other part of the network. The name is derived from the term "demilitarized zone", an area between nation states in which military operation is not permitted.
	Tunnel
In computer networks, a tunneling protocol allows a network user to access or provide a network service that the underlying network does not support or provide directly. One important use of a tunneling protocol is to allow a foreign protocol to run over a network that does not support that particular protocol; for example, running IPv6 over IPv4. Another important use is to provide services that are impractical or unsafe to be offered using only the underlying network services; for example, providing a corporate network address to a remote user whose physical network address is not part of the corporate network. Because tunneling involves repackaging the traffic data into a different form, perhaps with encryption as standard, a third use is to hide the nature of the traffic that is run through the tunnel.

2.	Explain how a Packet Filter works.
In case of iptables we have a set of rules which are chained in multiple chaines. 3 default chains exist: input/output/forward. Forwarding chain is used by routers. Input + Output by local processes running on the machine that received the packet. Basically we can define a couple of rules and chain them. These rules could specify that certain traffic should be blocked/rejected based on characteristics as:
	protocol in general
	port
	source/destination ip
	simply all traffic
	protocol specific, e.g. block SYN requests of TCP

3.	Explain what Network Address Translation is and how it works.
NAT is typically implemented in router sotware. Translates a set of private ip addresses to one public ip address. This is done via mapping public-source-port->private-ip-address.
Possible configurations: 
incoming public:any-port traffic goes to private:any-port.
incoming public:80 goes to private:80

NATing also works witl traffic to host forwrding for incoming connections. Important configuratio:
full cone: allow any incoming connection to private host port
restricted cone: allow connections for known public hosts to private host port
restricted port: allow connections for known public host and known port to private host 
known means: previously traffic occured in direction private->public.

4.	Describe inherent weaknesses and vulnerabilities in the concept “Firewall”.
Attacks from inside not avoidable/blockable.
Tunnelling could be used to bypass rules.

5.a.	 Describe how a static packet filter distinguishes between incoming and outgoing TCPconnections. Denote the corresopnding rules in pseudonotation or verbally.
ncoming TCP packet acceptance rules can make use of the connection state flags associated with TCP connections. All TCP connections adhere to the same set of connection states. These states differ between client and server because of the three-way handshake during connection establishment. As such, the firewall can distinguish between incoming traffic from remote clients and incoming traffic from remote servers.
Incoming TCP packets from remote clients will have the SYN flag set in the first packet received as part of the three-way connection establishment handshake. The first connection request will have the SYN flag set, but not the ACK flag.
Incoming packets from remote servers will always be responses to the initial connection request initiated from your local client program. Every TCP packet received from a remote server will have the ACK flag set. Your local client firewall rules will require all incoming packets from remote servers to have the ACK flag set. Servers do not normally attempt to initiate connections to client programs.

5.b.	Describe how a dynamic packet filter distinguishes between incoming and outgoing
TCP-connections. Denote the corresopnding rules in pseudonotation or verbally.
Dynamic packet filtering tracks the outgoing packets it has allowed to pass and allows only the corresponding response packets to return. When the first packet is transmitted to the public network (Internet), a reverse filter is dynamically created to allow the response packet to return. To be counted as a response, the incoming packet must be from the host and port to which the outbound packet was sent.
Stateful packet filtering supports both connection and connectionless protocols (TCP, UDP, ICMP, and so on). Dynamic packet filtering monitors each connection and creates a temporary (time-limited) inbound filter exception for the connection. This allows you to block incoming traffic originating from a particular port number and address while still allowing return traffic from that same port number and address.
This information is stored in a table, which is compared against the reply. If an incoming message is not a reply to the original request, then it is dropped. This dynamically created filter set is used to determine the subsequent packet transfers until the connection is closed.
For connection-oriented protocols, such as TCP, the incoming (reverse) filter is created only when the first outgoing packet is detected. TCP ACK bit filtering is automatically enabled on the reverse filter to prevent any connection attempts by intruders from being initiated through that filter.


